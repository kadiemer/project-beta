from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100, null=True)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.first_name


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    customer = models.CharField(max_length=200)
    date_time = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        null=True
        )
    service_type = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, )
    status = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_list_appointments", kwargs={"pk": self.pk})

    def __str__(self):
        return self.customer

    class Meta:
        ordering = ("customer", "vin")
