from django.urls import path
from .views import (
    api_show_appointments,
    api_list_technicians,
    api_list_appointments)

urlpatterns = [
    path(
        "appointments/",
        api_list_appointments,
        name="api_list_appointments"
        ),
    path(
        "vin/<int:vin_id>/appointments/",
        api_list_appointments,
        name="api_list_vin"
        ),
    path(
        "appointments/<int:pk>/",
        api_show_appointments,
        name="api_show_appointments"
        ),
    path(
        "technicians/",
        api_list_technicians,
        name="api_technicians"
        ),
    path(
        "history/",
        api_show_appointments,
        name="api_history"
        ),

]
