from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "customer",
        "date_time",
        "service_type",
        "vin",
        "status",
        "technician"
    ]
    encoders = {
        "technician": TechnicianDetailEncoder(),
    }
