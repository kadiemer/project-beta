from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Technician, Appointment
import json
from .encoders import (
    TechnicianDetailEncoder,
    AutomobileVODetailEncoder,
    AppointmentListEncoder
    )


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentListEncoder
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    else:
        content = json.loads(request.body)
        try:
            tech = content["technician"]
            tech = Technician.objects.get(first_name=tech)
            content["technician"] = tech
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Invalid tech id"})
            response.status_code = 404
            return response

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointments(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            Appointment.objects.filter(id=pk).delete()
            return JsonResponse({"Delete": True})
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            appointment_complete = Appointment.objects.get(id=pk)
            appointment_complete.status = True
            appointment_complete.save()
            return JsonResponse(
                appointment_complete, encoder=AppointmentListEncoder, safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response




@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        try:
            technician = Technician.objects.all()
            return JsonResponse(
                {"technicians": technician},
                encoder=TechnicianDetailEncoder,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician does not exist"})
            response.status_code = 404
            return response
