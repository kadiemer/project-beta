from django.db import models
from django.urls import reverse


# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)
    sold = models.CharField(max_length=200)


class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12)

    def __str__(self):
        return self.first_name, " ", self.last_name

    def get_api_url(self):
        return reverse("detail_customers", kwargs={"pk": self.pk})


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.first_name, " ", self.last_name

    def get_api_url(self):
        return reverse("detail_salespersons", kwargs={"pk": self.pk})


class Sale(models.Model):
    price = models.CharField(max_length=20)

    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.PROTECT,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.PROTECT,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("detail_sales", kwargs={"pk": self.pk})
