from django.urls import path
from .views import (
    api_sale,
    api_sales,
    api_customers,
    api_customer,
    api_salespersons,
    api_salesperson,
)

urlpatterns = [
    path("sales/", api_sales, name="api_sales"),
    path("sales/<int:pk>/", api_sale, name="api_sale"),
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:pk>/", api_customer, name="api_customer"),
    path("salespeople/", api_salespersons, name="api_salespersons"),
    path("salespeople/<int:pk>/", api_salesperson, name="api_saleperson"),
]
