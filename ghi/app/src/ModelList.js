import React, {useEffect, useState} from 'react';

function ModelList(){
    const [models, setModels] = useState([]);

    const loadModels = async () => {
        const response = await fetch ('http://localhost:8100/api/models');
        if (response.ok){
            const data = await response.json();
            setModels(data.models);
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        loadModels();
    }, []);

    return (
        <div>
            <br></br>
            <table className = "table table-striped table-hover">
                <thead className="table-dark">
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return(
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td><img src={model.picture_url} width="170" height="100" alt="img"/></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ModelList;
