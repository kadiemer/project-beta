import React, {useEffect, useState} from 'react';


function TechForm(){
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setId] = useState('');



    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleIdChange = (event) => {
        const value = event.target.value;
        setId(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data={}
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;
        const url = 'http://localhost:8080/api/technicians/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if(response.ok) {
            event.target.reset();
            setFirstName('');
            setLastName('');
            setId('');

        }

    }
    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new technicians?</h1>
                <form onSubmit={(event) => handleSubmit(event)} id="create-tech-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleFirstNameChange} placeholder="Tech first name" required type="text" name="first_name" id="first_name" className="form-control" />
                    <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleLastNameChange} placeholder="Tech last name" required type="text" name="last_name" id="last_name" className="form-control" />
                    <label htmlFor="last_name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleIdChange} placeholder="Fabric" required type="text" name="employee_id" id="employee_id" className="form-control" />
                    <label htmlFor="employee_id">employee id</label>
                    </div>
                    <button className="btn btn-primary">Create Tech</button>
                </form>
                </div>
            </div>
        </div>
    )
}
export default TechForm;
