import React, {useEffect, useState} from 'react';

function ModelForm(){
    const[manufacturers, setManufacturers] = useState([]);
    const[name, setName] = useState('');
    const[picture_url, setPicture] = useState('');
    const[manufacturer, setManufacturer] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(()=> {
        fetchData();
    }, []);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleManuChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data={}
        data.name = name;
        data.manufacturer_id = manufacturer;
        data.picture_url = picture_url;

        const url='http://localhost:8100/api/models/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(url, fetchConfig);

        if (response.ok){
            event.target.reset();
            setName('');
            setManufacturer('');
            setPicture('');
        }

    }

    return(
        <div className="row">
            <div className = "offset-3 col-6">
                <div className = "shadow p-4 mt-4">
                    <h1>Create a New Model:</h1>
                    <form onSubmit={(event) => handleSubmit(event)} id="create-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture Url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleManuChange} required name="manufacturer" id="manufacturer" className="form-select">
                                <option value="">Select a Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ModelForm;
