import React, {useEffect, useState} from 'react';

function SalesList(){
    const [sales, setSales] = useState([]);
    const handleDeleteSale = async(event,saleId) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/sales/${saleId}`
        const fetchConfig = {
            method: "DELETE"
        }
        const response = await fetch(url, fetchConfig);
        if(response.ok){
            loadSales();
        }
    }
    const loadSales = async () => {
        const response = await fetch ('http://localhost:8090/api/sales');
        if (response.ok){
            const data = await response.json();
            setSales(data.sales);
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        loadSales();
    }, []);

    return (
        <div>
            <br></br>
            <table className = "table table-striped table-hover">
                <thead className = "table-dark">
                    <tr>
                        <th>Price</th>
                        <th>Customer</th>
                        <th>Salesperson</th>
                        <th>Automobile Vin</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return(
                            <tr key={sale.id}>
                                <td>{sale.price}</td>
                                <td>{sale.customer.first_name + ' ' + sale.customer.last_name}</td>
                                <td>{sale.salesperson.first_name + ' ' + sale.salesperson.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td><button type="button" className="btn btn-outline-danger" onClick={(event) => {
                                        handleDeleteSale(event, sale.id)
                                    }}>Delete</button>
                                    </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}


export default SalesList;
