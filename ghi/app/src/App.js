import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespeopleList from './SalespeopleList.js'
import SalesForm from './SalesForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalespeopleForm from './SalespeopleForm';
import SalesList from './SalesList';
import SalespersonHistory from './SalespersonHistory';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import TechForm from './TechForm';
import TechList from './TechList';
import AppointmentsForm from './AppointmentsForm';
import AppointmentList from './AppointmentsList';
import AppointmentHistory from './ServiceHistory';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" element={<ManufacturerList/>}/>
          <Route path="manufacturers/new" element={<ManufacturerForm/>}/>
          <Route path="models" element={<ModelList/>}/>
          <Route path="models/new" element={<ModelForm/>}/>
          <Route path="salespeople" element={<SalespeopleList/>}/>
          <Route path="/salespeople/new" element={<SalespeopleForm/>}/>
          <Route path="sales" element={<SalesList />}/>
          <Route path="/sales/new" element={<SalesForm/>}/>
          <Route path="sales/history" element={<SalespersonHistory/>}/>
          <Route path="customers" element={<CustomerList />}/>
          <Route path="/customers/new" element={<CustomerForm/>}/>
          <Route path="/technicians/new" element={<TechForm />}/>
          <Route path="technicians" element={<TechList />}/>
          <Route path="/appointments/new" element={<AppointmentsForm />}/>
          <Route path="appointments" element={<AppointmentList />}/>
          <Route path="history" element={<AppointmentHistory />}/>
          <Route path="/automobiles/new" element={<AutomobileForm />}/>
          <Route path="automobiles" element={<AutomobileList />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
