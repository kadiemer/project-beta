# CarCar

Team:

* Kadie Meroney - Sales Microservice
* Matthew Rauschenberg - Service Microservice

## Design

## Service microservice

I have a Technician model that is used to create a technician that you can assign to the appointments created from the appointment model. When listing appointments after creation I compare my vin that was input on the form with the vins that are in the inventory if a match comes back that appointment becomes a vip appointment.

## Sales microservice

The Sales Microservice implements a RESTful Api with models for Sale, Customer, Salesperson, and AutomobileVO. The AutomobileVO's data comes from the existing Inventory microservice via polling service. The poller checks for new Automobiles in the Inventory API every 60 seconds and updates the AutomobileVO model as necessary. Using React on the front-end, a single page application was implemented to allow users to view, create, and delete the various parts of the Sales Microservice.
